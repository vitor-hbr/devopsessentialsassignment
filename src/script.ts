interface result {
  title: string;
  message: string;
}

document.addEventListener("DOMContentLoaded", function () {
  const button = document.querySelector(".calculate-button");
  const input: HTMLInputElement = document.querySelector(".calculate-input");
  const title: HTMLElement = document.querySelector(".result-title");
  const message: HTMLElement = document.querySelector(".result-content");

  if (button) {
    button.addEventListener("click", () => {
      const number = Number(input.value);
      const res = calculateResult(number);
      title.innerHTML = res.title;
      message.innerHTML = res.message;
      if (res.title === "Error") {
        title.style.color = "#7859ff";
      } else {
        title.style.color = "#9eff4a";
      }
    });
  }
});

const calculateResult = (number: number) => {
  const result: result = { title: "", message: "" };
  if (number < 0) {
    result.title = "Error";
    result.message = "The chosen number can't be negative";
  } else {
    result.title = "Success";
    result.message = "The result is: " + Math.sqrt(number);
  }
  return result;
};
